import json
import os

from flask import Flask, jsonify

SECRET_NUMBER = os.environ.get('SECRET_NUMBER', '{"secret_number":123}')
SECRET_NUMBER = json.loads(SECRET_NUMBER)['secret_number']

PORT = os.environ.get('PORT', 5010)

app = Flask(__name__)


@app.route("/get_secret_number")
def index():
    return jsonify({'secret_number': SECRET_NUMBER})


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=PORT, debug=False)
