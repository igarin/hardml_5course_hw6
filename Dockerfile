FROM python:3.8-alpine
#RUN apk update \
#    && apk add --no-cache bash git build-base protobuf protoc libpq curl unzip make \
#    && apk add --virtual .build-deps gcc python-dev musl-dev postgresql-dev

COPY ./requirements.txt /app/requirements.txt
WORKDIR /app/
RUN pip install -r requirements.txt

COPY ./ /app

ARG SECRET_NUMBER='{"secret_number":123}'
ARG PORT

ENV PORT=${PORT}
ENV SECRET_NUMBER=${SECRET_NUMBER}

EXPOSE ${PORT}

CMD ["python","app.py"]